#include <bits/stdc++.h>
#include <ncurses.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#include <termios.h>
#include "device_communication.hpp"


#include "main_window.hpp"

int ch = 0;
bool running = 1;

void cleanup();

bool init() {
  // init curses
  initscr();

  // raw input
  raw();
  noecho();
  keypad(stdscr, TRUE);

  // wait for 100ms when reading
  timeout(100);
  
  return 1;
}

void cleanup() {
  endwin();
}

int main() {
  if (!init()) {
    return 1;
  }
  
  main_window m_wnd;

  m_wnd.draw_screen();
  
  while (running) {
    int tmp = getch();
    if (tmp >= 0) {
      ch = tmp;
      switch (ch) {
        
      case 3: // ctrl+c
      case 4: // ctrl+d
        running = 0;
        break;
        
      default:
        m_wnd.handle_key(ch);
        break;
      }
    }

    m_wnd.draw_screen();
  }
  
  endwin();
  return 0;
}

