#ifndef DEVICE_COMMUNICATION_HPP
#define DEVICE_COMMUNICATION_HPP

#include <cstdint>
#include <unistd.h>
#include <poll.h>

#include "firmware/usb_message.h"

const std::string port = "/dev/serial/by-id/usb-Teensyduino_USB_Serial_8348840-if00";

const int temp_count = 5;
const int pwm_count = 9;
const int sense_count = 11;

// timeout for device communication
const int timeout_ms = 1000;

struct temp_calibration {
  float a0, a1, a2, a3;  // curve fit parameters for 1/T = a0 + ln(R) a1 + ln^2(R) a2 + ln^3(R) a3
  float R_ref;           // reference resistor value [k]
};

struct fancurve {
  const static int32_t steps = 6;  // number of points in curve
  float temps[steps];              // temperatures (in °C) of control points
  float speeds[steps];             // fan speeds (in [0,1]) for control points
};

struct profile {
  char name[16];                                      // some string describing the profile
  
  temp_calibration temp_calibrations[temp_count];     // calibration values for temperature sensors
  float temp_moving_average_factor;                   // weight of old temp value for moving average
  
  int32_t source_temp[pwm_count];                     // temperature sensor to use for PWM control
  fancurve fancurves[pwm_count];                      // fancurves for sensors

  int32_t rpm_pulses_per_revolution[sense_count];     // number of signal changes per one revolution
  float rpm_moving_average_factor;                    // weight of old rpm value for moving average
  uint32_t rpm_max_time;                              // max time to wait inbetween pulses [µs] (no pulse for longer is rounded to 0 rpm)
  uint32_t rpm_min_time;                              // min time to wait inbetween pulses [µs] (for debouncing)
};

struct dynamic_state {
  int32_t temp_raw[temp_count];                       // values coming from adc of temp pins
  float temp[temp_count];                             // corresponding temperatures measured
  
  int pwm_vals[pwm_count];                            // current pwm values
  
  float rpms[sense_count];                            // current fan rpms
  uint32_t last_sense_change[sense_count];            // time of last sense pin change
};

std::string message_string(usb_message m) {
  switch (m) {
  case msg_get_dynamic_state:
    return "msg_get_dynamic_state";
  case msg_send_dynamic_state:
    return "msg_send_dynamic_state";
  case msg_get_profile:
    return "msg_get_profile";
  case msg_send_profile:
    return "msg_send_profile";
  case msg_ack:
    return "msg_ack";
  case msg_move_profile:
    return "msg_move_profile";
  case msg_get_active_slot:
    return "msg_get_active_slot";
  case msg_send_active_slot:
    return "msg_send_active_slot";
  case msg_nack:
    return "msg_nack";
  case msg_error:
    return "msg_error";
  default:
    return "unknown: " + std::to_string((int)m);
  }
}

// get current time in ms
long long now_ms() {
  return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}

// error message if a device access function failed
std::string device_communication_error;

// test communication by sending an error message, which should be answered by another error message
int read_timeout(int device, char *buf, int len, int timeout_ms) {
  // if timeout is negative, wait infinitely
  if (timeout_ms < 0)
    return read(device, buf, len);
  
  // time of function call and time left over to read
  long long start_time = now_ms(), left_time = timeout_ms;
  // struct used for poll
  pollfd fd = {device, POLLIN, 0};
  // number of bytes read
  int read_len = 0;
  // read as long as buffer is not full and there is time left
  while (read_len < len && left_time >= 0) {
    // wait for read availability, max timeout
    poll(&fd, 1, left_time);
    // if there is something to read, read it
    if (fd.revents & POLLIN)
      read_len += read(device, buf + read_len, len - read_len);
    // update leftover time
    left_time = start_time + timeout_ms - now_ms();
  }
  // return number of bytes successfully read
  return read_len;
}

bool test_communication(int device) {
  // assemble message
  int msglen = sizeof(usb_message);
  char message[msglen];
  *((usb_message*)message) = msg_error;

  // send message
  if (write(device, message, msglen) != msglen) {
    device_communication_error = "test_communication failed to write";
    return 0;
  }
  
  // prepare answer buffer
  int anslen = sizeof(usb_message);
  char answer[anslen];

  // receive answer
  if (read_timeout(device, answer, anslen, timeout_ms) != anslen) {
    device_communication_error = "test_communication failed to read";
    return 0;
  }

  // extract answer components
  usb_message answer_message = *((usb_message*)answer);

  // check for correct answer
  if (answer_message != msg_error) {
    device_communication_error = "test_communication received incorrect answer: " + message_string(answer_message);
    return 0;
  }

  return 1;
}

bool get_dynamic_state(dynamic_state &d, int device) {
  // assemble message
  int msglen = sizeof(usb_message);
  char message[msglen];
  *((usb_message*)message) = msg_get_dynamic_state;

  // send message
  if (write(device, message, msglen) != msglen) {
    device_communication_error = "get_dynamic_state failed to write";
    return 0;
  }
  
  // prepare answer buffer
  int anslen = sizeof(usb_message) + sizeof(dynamic_state);
  char answer[anslen];

  // receive answer
  if (read_timeout(device, answer, anslen, timeout_ms) != anslen) {
    device_communication_error = "get_dynamic_state failed to read";
    return 0;
  }

  // extract answer components
  usb_message answer_message = *((usb_message*)answer);
  dynamic_state answer_payload = *((dynamic_state*)(answer + sizeof(usb_message)));

  // check for correct answer
  if (answer_message != msg_send_dynamic_state) {
    device_communication_error = "get_dynamic_state received incorrect answer: " + message_string(answer_message);
    return 0;
  }

  // set return value
  d = answer_payload;

  return 1;
}

bool get_profile(int slot, profile &p, int device) {
  // assemble message
  int msglen = sizeof(usb_message) + sizeof(int);
  char message[msglen];
  *((usb_message*)message) = msg_get_profile;
  *((int*)(message + sizeof(usb_message))) = slot;

  // send message
  if (write(device, message, msglen) != msglen) {
    device_communication_error = "get_profile failed to write";
    return 0;
  }

  // prepare answer buffer
  int anslen = sizeof(usb_message) + sizeof(int) + sizeof(profile);
  char answer[anslen];

  // receive answer
  if (read_timeout(device, answer, anslen, timeout_ms) != anslen) {
    device_communication_error = "get_profile failed to read";
    return 0;
  }

  // extract answer components
  usb_message answer_message = *((usb_message*)answer);
  int answer_slot = *((int*)(answer + sizeof(usb_message)));
  profile answer_profile = *((profile*)(answer + sizeof(usb_message) + sizeof(int)));

  // check for correct answer
  if (answer_message != msg_send_profile || answer_slot != slot) {
    device_communication_error = "get_profile received incorrect answer: " + message_string(answer_message);
    return 0;
  }

  // set return value
  p = answer_profile;

  return 1;
}

bool set_profile(int slot, profile p, int device) {
  // assemble message
  int msglen = sizeof(usb_message) + sizeof(int) + sizeof(profile);
  char message[msglen];
  *((usb_message*)message) = msg_send_profile;
  *((int*)(message + sizeof(usb_message))) = slot;
  *((profile*)(message + sizeof(usb_message) + sizeof(int))) = p;

  // send message
  if (write(device, message, msglen) != msglen) {
    device_communication_error = "set_profile failed to write";
    return 0;
  }

  // prepare answer buffer
  int anslen = sizeof(usb_message);
  char answer[anslen];

  // receive answer
  if (read_timeout(device, answer, anslen, timeout_ms) != anslen) {
    device_communication_error = "set_profile failed to read";
    return 0;
  }

  // extract answer components
  usb_message answer_message = *((usb_message*)answer);
  
  // check for correct answer
  if (answer_message != msg_ack) {
    device_communication_error = "set_profile received incorrect answer: " + message_string(answer_message);
    return 0;
  }

  return 1;  
}

bool move_profile(int dest, int source, int device) {
  // assemble message
  int msglen = sizeof(usb_message) + sizeof(int) + sizeof(int);
  char message[msglen];
  *((usb_message*)message) = msg_move_profile;
  *((int*)(message + sizeof(usb_message))) = dest;
  *((int*)(message + sizeof(usb_message) + sizeof(int))) = source;

  // send message
  if (write(device, message, msglen) != msglen) {
    device_communication_error = "move_profile failed to write";
    return 0;
  }

  // prepare answer buffer
  int anslen = sizeof(usb_message);
  char answer[anslen];

  // receive answer
  if (read_timeout(device, answer, anslen, timeout_ms) != anslen) {
    device_communication_error = "move_profile failed to read";
    return 0;
  }

  // extract answer components
  usb_message answer_message = *((usb_message*)answer);
  
  // check for correct answer
  if (answer_message != msg_ack) {
        device_communication_error = "move_profile received incorrect answer: " + message_string(answer_message);
    return 0;
  }

  return 1;
}

bool get_active_slot(int &slot, int device) {
  // assemble message
  int msglen = sizeof(usb_message);
  char message[msglen];
  *((usb_message*)message) = msg_get_active_slot;

  // send message
  if (write(device, message, msglen) != msglen) {
    device_communication_error = "get_active_slot failed to write";
    return 0;
  }

  // prepare answer buffer
  int anslen = sizeof(usb_message) + sizeof(int);
  char answer[anslen];

  // receive answer
  if (read_timeout(device, answer, anslen, timeout_ms) != anslen) {
    device_communication_error = "get_active_slot failed to read";
    return 0;
  }
    
  // extract answer components
  usb_message answer_message = *((usb_message*)answer);
  int answer_slot = *((int*)(answer + sizeof(usb_message)));
  
  // check for correct answer
  if (answer_message != msg_send_active_slot) {
    device_communication_error = "get_active_slot received incorrect answer: " + message_string(answer_message);
    return 0;
  }

  // set return value
  slot = answer_slot;
  
  return 1;
}

bool set_active_slot(int slot, int device) {
  // assemble message
  int msglen = sizeof(usb_message) + sizeof(int);
  char message[msglen];
  *((usb_message*)message) = msg_send_active_slot;
  *((int*)(message + sizeof(usb_message))) = slot;

  // send message
  if (write(device, message, msglen) != msglen) {
    device_communication_error = "set_active_slot failed to write";
    return 0;
  }

  // prepare answer buffer
  int anslen = sizeof(usb_message);
  char answer[anslen];

  // receive answer
  if (read_timeout(device, answer, anslen, timeout_ms) != anslen) {
    device_communication_error = "set_active_slot failed to read";
    return 0;
  }

  // extract answer components
  usb_message answer_message = *((usb_message*)answer);
  
  // check for correct answer
  if (answer_message != msg_ack) {
    device_communication_error = "set_active_slot received incorrect answer: " + message_string(answer_message);
    return 0;
  }
  
  return 1;  
}

void print(dynamic_state s) {
  std::cout << "raw temps: ";
  for (int i = 0; i < temp_count; i++)
    std::cout << s.temp_raw[i] << " ";
  std::cout << "\n";
  
  std::cout << "temps: ";
  for (int i = 0; i < temp_count; i++)
    std::cout << s.temp[i] << " ";
  std::cout << "\n";

  std::cout << "pwms: ";
  for (int i = 0; i < pwm_count; i++)
    std::cout << s.pwm_vals[i] << " ";
  std::cout << "\n";

  std::cout << "rpms: ";
  for (int i = 0; i < sense_count; i++)
    std::cout << s.rpms[i] << " ";
  std::cout << "\n";
}

void print(profile p) {
  std::cout << "name: " << p.name << "\n";

  for (int i = 0; i < temp_count; i++) {
    auto t = p.temp_calibrations[i];
    std::cout << "temp calibration " << i
              << ": a = ["
              << t.a0 << " " << t.a1 << " " << t.a2 << " " << t.a3
              << "], R_ref = "
              << t.R_ref
              << "\n";
  }

  std::cout << "temp moving average factor: " << p.temp_moving_average_factor << "\n";
  
  for (int i = 0; i < pwm_count; i++) {
    auto f = p.fancurves[i];
    std::cout << "fan " << i << ": "
              << "temp source = " << p.source_temp[i] << ", "
              << "fancurve = [[" << f.temps[0];
    for (int i = 1; i < f.steps; i++)
      std::cout << " " << f.temps[i];
    std::cout << "],[" << f.speeds[0];
    for (int i = 1; i < f.steps; i++)
      std::cout << " " << f.speeds[i];
    std::cout << "]]\n";
  }
  
  std::cout << "rpm pulses per revolution: ";
  for (int i = 0; i < sense_count; i++)
    std::cout << p.rpm_pulses_per_revolution[i] << " ";
  std::cout << "\n";
  
  std::cout << "rpm moving average factor: " << p.rpm_moving_average_factor << "\n";
  
  std::cout << "rpm max time: " << p.rpm_max_time << "\n";
  std::cout << "rpm min time: " << p.rpm_min_time << "\n";
}
  

#endif
