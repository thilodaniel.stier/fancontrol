
CFLAGS = -std=c++17 -o2 -Wall
LDFLAGS = -lncurses


Fancontrol: main.cpp device_communication.hpp main_window.hpp profile_edit_window.hpp
	g++ $(CFLAGS) -o Fancontrol main.cpp $(LDFLAGS)

CommunicationTest : communication_test.cpp device_communication.hpp
	g++ $(CFLAGS) -o communication_test communication_test.cpp $(LDFLAGS)

all: Fancontrol CommunicationTest

ctest: all
	./communication_test

test: all
	./Fancontrol

install: all
	ln -s $(shell pwd)/Fancontrol /usr/bin/fans

uninstall:
	rm /usr/bin/fans

clean:
	rm -f Fancontrol
	rm -f communication_test
	rm -f *~
	rm -f firmware/*~
