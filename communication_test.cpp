#include <bits/stdc++.h>
#include <stdio.h>
#include <string.h>


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "device_communication.hpp"

char serialPortFilename[] = "/dev/ttyACM0";

int main()
{
  
  int device = open(serialPortFilename, O_RDWR);

  if (!device) {
    std::cerr << "error opening device\n";	
    return 0;
  }

  std::cout << "opened " << serialPortFilename << "\n";


  dynamic_state s;

  std::cout << "######################### get dynamic state ##########################" << std::endl;
  if (get_dynamic_state(s, device))
    std::cout << "success!" << std::endl;
  print(s);
  
  profile p;
  std::cout << "######################### get profile ##########################" << std::endl;
  if (get_profile(-1, p, device))
    std::cout << "success!" << std::endl;
  print(p);

  std::cout << "######################### get profile ##########################" << std::endl;
  if (get_profile(0, p, device))
    std::cout << "success!" << std::endl;

  std::cout << "######################### set profile ##########################" << std::endl;
  if (set_profile(-1, p, device))
    std::cout << "success!" << std::endl;

  std::cout << "######################### get profile ##########################" << std::endl;
  if (get_profile(-1, p, device))
    std::cout << "success!" << std::endl;
  print(p);

  std::cout << "######################### move profile ##########################" << std::endl;
  if (move_profile(-1, -2, device))
    std::cout << "success!" << std::endl;

  std::cout << "######################### get profile ##########################" << std::endl;
  if (get_profile(-1, p, device))
    std::cout << "success!" << std::endl;
  print(p);
  
  
  std::cout << "######################### set active slot ##########################" << std::endl;
  if (set_active_slot(2, device))
    std::cout << "success!" << std::endl;
  
  int slot;
  std::cout << "######################### get active slot ##########################" << std::endl;
  if (get_active_slot(slot, device))
    std::cout << "success!" << std::endl;
  std::cout << "active slot = " << slot << "\n";

  std::cout << "######################### set active slot ##########################" << std::endl;
  if (set_active_slot(3, device))
    std::cout << "success!" << std::endl;

  std::cout << "######################### get active slot ##########################" << std::endl;
  if (get_active_slot(slot, device))
    std::cout << "success!" << std::endl;
  std::cout << "active slot = " << slot << "\n";
  
  
  
  
  
  
  return 0;
}
