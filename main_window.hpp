#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP

#include <bits/stdc++.h>
#include <ncurses.h>
#include "device_communication.hpp"
#include "profile_edit_window.hpp"

class main_window {
  int device;

  // cache for device variables that do not change on their own:
  profile profiles[14];
  int active_profile_slot;
  
  // slot selected to copy if within range [-4, 9]
  int copy_selected_slot = -5;

  // slot selected for profile editing if within range [-4, 9] (go into edit mode if that is the case)
  int profile_edit_slot = -5;
  
  // window opened when going into edit mode for a profile
  profile_edit_window pe_wnd;

  // (user) cursor position
  int cursor_x = 68, cursor_y = 5;
  int virtual_cursor_x = 68;

  // positions where the (user) cursor is allowed to be
  std::bitset<80> allowed_positions[24] =
    {std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000100010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000100010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000100010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000100010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000100010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000100010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000100010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000100010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000100010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000100010001000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00010000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80)};

  void move_cursor_up() {
    int cur_x = cursor_x, cur_y = cursor_y;
    // go up one and put cursor on virtual x
    cursor_y = (cursor_y + 23) % 24;
    cursor_x = virtual_cursor_x;
    // search left and then up for allowed position
    while (!allowed_positions[cursor_y][79 - cursor_x]) {
      if (cursor_x == 0) 
        cursor_x = virtual_cursor_x, cursor_y = (cursor_y + 23) % 24;
      else
        cursor_x--;
    }
    // if we landed where we started, we want to search right instead of left
    if (cursor_x == cur_x && cursor_y == cur_y) {
      // go up one and put cursor on virtual x
      cursor_y = (cursor_y + 23) % 24;
      cursor_x = virtual_cursor_x;
      // search right and then up for allowed position
      while (!allowed_positions[cursor_y][79 - cursor_x]) {
        if (cursor_x == 79) 
          cursor_x = virtual_cursor_x, cursor_y = (cursor_y + 23) % 24;
        else
          cursor_x++;
      }
      // set virtual x to found position to be able to search up nicely
      virtual_cursor_x = cursor_x;
    }
  }
  void move_cursor_down() {
    int cur_x = cursor_x, cur_y = cursor_y;
    // go down one and put cursor on virtual x
    cursor_y = (cursor_y + 1) % 24;
    cursor_x = virtual_cursor_x;
    // search left and then down for allowed position
    while (!allowed_positions[cursor_y][79 - cursor_x]) {
      if (cursor_x == 0)
        cursor_x = virtual_cursor_x, cursor_y = (cursor_y + 1) % 24;
      else
        cursor_x--;
    }
    // if we landed where we started, we want to search right instead of left
    if (cursor_x == cur_x && cursor_y == cur_y) {
      // go up one and put cursor on virtual x
      cursor_y = (cursor_y + 1) % 24;
      cursor_x = virtual_cursor_x;
      // search right and then up for allowed position
      while (!allowed_positions[cursor_y][79 - cursor_x]) {
        if (cursor_x == 79) 
          cursor_x = virtual_cursor_x, cursor_y = (cursor_y + 1) % 24;
        else
          cursor_x++;
      }
      // set virtual x to found position to be able to search up nicely
      virtual_cursor_x = cursor_x;
    }
  }
  void move_cursor_left() {
    // go left until valid position is found; reset virtual x to real x
    while (!allowed_positions[cursor_y][79 - (virtual_cursor_x = cursor_x = (cursor_x + 79) % 80)]) ;
  }
  void move_cursor_right() {
    // go right until valid position is found; reset virtual x to real x
    while (!allowed_positions[cursor_y][79 - (virtual_cursor_x = cursor_x = (cursor_x + 1) % 80)]) ;
  }
  
  std::string temp_names[temp_count] = {"Temp 0", "Temp 1", "Temp 2", "Temp 3", "Temp 4"};
  std::string fan_names[sense_count] = {"Fan 0", "Fan 1","Fan 2","Fan 3","Fan 4","Fan 5","Fan 6","Fan 7","Fan 8", "Pump 0", "Pump 1"};


  // is the device available for communication?
  bool device_available;

  // error messages
  std::vector<std::string> errors;
  // show errors on screen (instead of normal content)
  bool show_errors = 0;
  
  // time [ms] when device was lost or last connection attempt was made
  long long lost_time;
  // number in ms after which we try to reconnect to device
  long long retry_timeout = 5000;
  
  // open and initialize the serial device
  void open_device() {
    // set time for connection attempt
    lost_time = now_ms();
    
    // open serial device, read-write and non-blocking
    device = open(port.c_str(), O_RDWR | O_NONBLOCK);

    if (device < 0) {
      errors.push_back(std::string("could not open device: ") + strerror(errno));
      device_available = 0;
      return;
    }
  
    struct termios tty;

    if (tcgetattr(device, &tty) != 0) {
      errors.push_back("could not get device attributes");
      device_available = 0;
      close(device);
      return;
    }
    
    // set up flags like arduino serial monitor
    // todo: separate these flags into defined parts
    //*
    tty.c_cflag = 3261;
    tty.c_lflag = 2608;
    tty.c_iflag = 0;
    tty.c_oflag = 4;
    //*/

    if (tcsetattr(device, TCSANOW, &tty) != 0) {
      errors.push_back("could not set device attributes");
      device_available = 0;
      close(device);
      return;
    }
  
    // get exclusive access (does not apply to serial monitor for some reason)
    if (flock(device, LOCK_EX | LOCK_NB) == -1) {
      errors.push_back("device is already locked by another process");
      device_available = 0;
      close(device);
      return;
    }

    // test communication
    if (!test_communication(device)) {
      errors.push_back(device_communication_error);
      device_available = 0;
      close(device);
      return;
    }
  
    device_available = 1;
  }
  
  // function called when communication failes
  void handle_device_disconnect() {
    device_available = 0;
    lost_time = now_ms();
    close(device);
    errors.push_back("device connection lost");
  }
  
  // reload all cached data from device (if available)
  void reload_device_info() {
    if (!device_available)
      return;
    
    for (int i = -4; i < 10; i++)
      if (!get_profile(i, profiles[i+4], device)) {
        errors.push_back(device_communication_error);
        errors.push_back("get_profile failed");
        handle_device_disconnect();
        return;
      }
    
    if (!get_active_slot(active_profile_slot, device)) {
      errors.push_back(device_communication_error);
      errors.push_back("get_active_slot failed");
      handle_device_disconnect();
      return;
    }
  }
  
public:
  main_window() : pe_wnd(profiles[0], 0) {
    open_device();
    reload_device_info();
  }
  
  void draw_screen() {
    // clear screen
    clear();
    
    // if in profile edit mode, call the pe_wnd to draw instead
    if (profile_edit_slot >= -4 && profile_edit_slot <= 9) {
      pe_wnd.draw_screen();
      return;
    }
    
    // if errors should be shown instead of main window
    if (!device_available || show_errors) {
      // print errors to screen
      for (unsigned int i = 0; i < errors.size(); i++)
        mvaddstr(1 + i, 2, errors[i].c_str());

      // if device not available try to reconnect (keeping timeout in mind)
      if (!device_available && now_ms() > lost_time + retry_timeout)
        open_device();
      
      return;
    }
    
    // get up-to-date sensor information
    static dynamic_state d;
    // number of successive dynamic states lost
    static int lost_counter = 0;
    // get dynamic state and handle failure
    if (!get_dynamic_state(d, device)) {
      errors.push_back(device_communication_error);
      
      lost_counter++;

      // only if we fail more than 3 times in a row, handle it as a device loss
      if (lost_counter > 3) {
        handle_device_disconnect();
        return;
      }
    }
    else {
      lost_counter = 0;
    }

    // draw border
    {
      mvaddch(0,0,ACS_ULCORNER);
      mvaddch(0,80-1,ACS_URCORNER);
      mvaddch(24-1,0,ACS_LLCORNER);
      mvaddch(24-1,80-1,ACS_LRCORNER);
      for (int i = 1; i < 80-1; i++) {
        mvaddch(0, i, ACS_HLINE);
        mvaddch(24-1,i, ACS_HLINE);
      }
      for (int i = 1; i < 24-1; i++) {
        mvaddch(i, 0, ACS_VLINE);
        mvaddch(i, 80-1, ACS_VLINE);
      }
    }

    // add temp text
    {
      mvaddstr(1, 2, "Temps:");
      for (int i = 0; i < temp_count; i++) {
        mvaddstr(i+1, 9, (temp_names[i] + ':').c_str());
        if (d.temp[i] < -9.99 || d.temp[i] > 99.99)
          mvprintw(i+1, 17, " N/A ");
        else
          mvprintw(i+1, 17, "%2.2f °C", d.temp[i]);
      }
    }

    // draw temp/fan divider
    {
    mvaddch(6,0,ACS_LTEE);
    for (int i = 1; i < 36; i++)
      mvaddch(6,i,ACS_HLINE);
    }

    // draw fan text
    {
      mvaddstr(7, 2, "Fans:");
      for (int i = 0; i < pwm_count; i++) {
        mvprintw(i+7, 9, "%-7s%5.0f RPM %3.0f% PWM", (fan_names[i] + ':').c_str(), d.rpms[i], d.pwm_vals[i] / 40.96);
      }
    }
  
    // draw fan/pump divider
    {
      mvaddch(16,0,ACS_LTEE);
      for (int i = 1; i < 36; i++)
        mvaddch(16,i,ACS_HLINE);
    }

    // draw pump text
    {
      mvaddstr(17, 2, "Pumps:");
      for (int i = pwm_count; i < sense_count; i++) {
        mvprintw(i+8, 9, "%-7s%5.0f RPM", (fan_names[i] + ':').c_str(), d.rpms[i]);
      }
    }

    // draw profile text
    {
      mvaddstr(1, 38, "profiles:");
      
      for (int i = 0; i < 14; i++) {
        // get name and make sure it is finite (truncate after 14 characters)
        std::string name(profiles[i].name, 16);
        name[14] = '\0';
        // print number and name of profile
        mvprintw(i+1, 48, "%2d: %14s ", i-4, name.c_str());
        // if profile is selectable, print selection box
        if (i >= 4)
          mvprintw(i+1, 67, "[%c]", (i-4==active_profile_slot?'*':' '));
        // print [E]dit and [C]opy boxes
        mvprintw(i+1, 71, "[E] [C]");
      }

      // highlight copy selected slot (if there is one)
      if (copy_selected_slot >= -4 && copy_selected_slot <= 9)
        mvaddch(copy_selected_slot + 5, 76, A_BOLD | A_STANDOUT | 'C');
    }

    // draw footer divider
    {
      mvaddch(21,0,ACS_LTEE);
      for (int i = 1; i < 79; i++)
        mvaddch(21,i,ACS_HLINE);
      mvaddch(21,79,ACS_RTEE);
    }

    // draw vertical divider
    {
      for (int i = 1; i < 24-2; i++)
        mvaddch(i,36,ACS_VLINE);
      mvaddch(0,36,ACS_TTEE);
      mvaddch(6,36,ACS_RTEE);
      mvaddch(16,36,ACS_RTEE);
      mvaddch(24-3,36,ACS_BTEE);
    }

    // draw footer
    {
      mvprintw(22, 2, "[show errors]");
      // mvprintw(22, 16, "[save to file]");
      // mvprintw(22, 31, "[load from file]");
      // mvaddch(21, 48, ACS_TTEE);
      // mvaddch(22, 48, ACS_VLINE);
      // mvaddch(23, 48, ACS_BTEE);
    }
    
    // move cursor to previous position
    move(cursor_y, cursor_x);

    // refresh the screen
    refresh();
  }

  // handle arrow keys and enter to navigate window
  void handle_key(int key) {
    // if there is no device, don't do anything (for now)
    if (!device_available) {
      return;
    }

    // when showing errors, return to normal view
    if (show_errors) {
      show_errors = 0;
      return;
    }
    
    // if in profile edit mode, forward the keystroke to the edit window
    if (profile_edit_slot >= -4 && profile_edit_slot <= 9) {
      pe_wnd.handle_key(key);
      
      // check whether the pe_wnd is done and we should return to the main window
      if (pe_wnd.is_done()) {
        // set the edited profile
        if (!set_profile(profile_edit_slot, pe_wnd.get_profile(), device)) {
          errors.push_back(device_communication_error);
          handle_device_disconnect();
          return;
        }
        // update cache
        reload_device_info();
        // go out of profile edit mode
        profile_edit_slot = -5;
      }
      return;
    }
    
    switch (key) {
    case KEY_UP: {
      move_cursor_up();
    } break;
    case KEY_DOWN: {
      move_cursor_down();
    } break;
    case KEY_LEFT: {
      move_cursor_left();
    } break;
    case KEY_RIGHT: {
      move_cursor_right();
    } break;
    case ' ':
    case '\n': {

      // copy profile
      if (copy_selected_slot >= -4 && copy_selected_slot <= 9 &&
          cursor_x >= 48 && cursor_x <= 78 &&
          cursor_y >= 1 && cursor_y <= 14) {
        // copy from copy_selected_slot to (cursor_y - 5)
        if (cursor_y - 5 != copy_selected_slot) {
          if (!move_profile(cursor_y - 5, copy_selected_slot, device)) {
            errors.push_back(device_communication_error);
            handle_device_disconnect();
            copy_selected_slot = -5;
            return;
          }
          
          reload_device_info();
        }
        copy_selected_slot = -5;
      }
      // select active profile
      else if (cursor_x >= 67 && cursor_x <= 69 &&
               cursor_y >= 5 && cursor_y <= 14) {
        if (!set_active_slot(cursor_y - 5, device)) {
          errors.push_back(device_communication_error);
          handle_device_disconnect();
          return;
        }
        reload_device_info();
      }
      // edit profile
      else if (cursor_x >= 71 && cursor_x <= 73 &&
               cursor_y >= 1 && cursor_y <= 14) {
        // set the slot to edit
        profile_edit_slot = cursor_y - 5;
        // set the pe_wnd
        pe_wnd = profile_edit_window(profiles[profile_edit_slot + 4], profile_edit_slot);
      }
      // select profile to copy
      else if (cursor_x >= 75 && cursor_x <= 77 &&
               cursor_y >= 1 && cursor_y <= 14) {
        copy_selected_slot = cursor_y - 5;
      }
      // show errors
      else if (cursor_y == 22 && cursor_x == 3) {
        show_errors = 1;
      }
      // save to file
      else if (cursor_y == 22 && cursor_x == 17) {
        // todo: implement
      }
      // load from file
      else if (cursor_y == 22 && cursor_x == 32) {
        // todo: implement
      }
      
    } break;
    }
  }
};



#endif
