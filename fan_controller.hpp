// 

#ifndef FAN_CONTROLLER_HPP
#define FAN_CONTROLLER_HPP

#include <stdio.h>
#include <iostream>
#include <string>

class fan_controller {
  // buffer for reading data from teensy
  char serial_read_buffer[1024];

  const std::string serial_port_name;

  FILE *serial_port;
  
  fan_controller(std::string serial_port_name) :
    serial_port_name(serial_port_name) {

    serial_port = fopen(serial_port_name.c_str(), "rw");
    if (!serial_port) {
      std::cerr << "could not open serial port!\n";
    }
  }

  void read_serial() {
    if (!serial_port)
      return;

    fread(serial_read_buffer, sizeof(char), 1024, serial_port);
  }

public:
  
  
};


#endif
