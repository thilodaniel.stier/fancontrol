#include "profile.h"

#include "constants.h"
#include "rpm_sense.h"
#include "communication.h"

dynamic_state state;

void setup() {
  cli(); // don't handle interrupts during setup
  
  // set PWM frequencies
  for (int i = 0; i < pwm_count; i++)
    analogWriteFrequency(pwm_pins[i], pwm_freq);

  // set pwm resolution
  analogWriteResolution(pwm_res);

  // set up pullups for fan/pump sense inputs
  for (int i = 0; i < sense_count; i++)
    pinMode(sense_pins[i], INPUT_PULLUP);

  // set led pin to output
  pinMode(13, OUTPUT);

  // set analog input resolution and number of samples to average
  analogReadRes(adc_res);
  analogReadAveraging(adc_average_number);
  
  // set current state
  current_state = &state;

  // setup interrups for sense inputs
  setup_sense_interrupts();

  // load profiles stored in EEPROM
  copy_profile(0, -4);
  copy_profile(1, -3);
  copy_profile(2, -2);
  copy_profile(3, -1);
  
  // get initial values
  update_dynamic_state();
  
  Serial.begin(0); // start usb communication
  
  sei(); // enable interrupts
}


void loop() {

  // handle communications
  handle_usb();

  // update dynamic state
  update_dynamic_state();

  // turn off led signal if it is over time
  led_signal(-1);
  
}
