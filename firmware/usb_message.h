#ifndef USB_MESSAGE_H
#define USB_MESSAGE_H

enum usb_message : char {
  msg_get_dynamic_state = 'e',   // PC->FC, ask for dyn state: no payload
  msg_send_dynamic_state = 'd',  // FC->PC, send dyn state: dynamic_state payload
  msg_get_profile = 'q',         // PC->FC, ask for profile: int payload (slot number)
  msg_send_profile = 'p',        // both, send profile: int, profile payload (slot, profile)
  msg_ack = 'a',                 // FC->PC, confirm success: no payload
  msg_move_profile = 'm',        // PC->FC, move profile between slots: int,int payload (dest, src)
  msg_get_active_slot = 't',     // PC->FC, get active profile slot: no payload
  msg_send_active_slot = 's',    // both, send active profile slot: int payload (slot)
  msg_nack = 'n',                // FC->PC, answer to invalid command: no payload
  msg_error = '!',               // FC->PC, if message can not be interpreted
};

#endif


