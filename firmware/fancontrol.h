#ifndef FANCONTROL_H
#define FANCONTROL_H

// parameters of the fan curve
struct fancurve {
  const static int steps = 6;  // number of points in curve
  float temps[steps];          // temperatures (in °C) of control points
  float speeds[steps];         // fan speeds (in [0,1]) for control points
};

// evaluate the fan curve (to get pwm activation in [0,1] based on temp)
float evaluate_fancurve(const float &temp, const fancurve &curve) {
  // find next temperature point in fan curve (linear search)
  int i = 0;
  for (; i + 1 < curve.steps && curve.temps[i] < temp; i++);
  
  // temp is before first point -> return speed of first point 
  if (i == 0)
    return curve.speeds[0];

  // temp is after last point -> return speed of last point
  if (curve.temps[i] < temp)
    return curve.speeds[i];

  // temp is between two points -> interpolate linearly
  float a = (temp - curve.temps[i - 1]) / (curve.temps[i] - curve.temps[i - 1]);
  return curve.speeds[i - 1] * (1 - a) + curve.speeds[i] * a;
}

#endif
