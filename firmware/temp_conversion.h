#ifndef TEMP_CONVERSION_H
#define TEMP_CONVERSION_H

// parameters for thermocouple calibration
struct temp_calibration {
  float a0, a1, a2, a3;  // curve fit parameters for 1/T = a0 + ln(R) a1 + ln^2(R) a2 + ln^3(R) a3
  float R_ref;           // reference resistor value [k]
};

const float T0 = 273.15;                  // Kelvin to Celsius offser

// calculate temperature (in °C) based on adc reading
float analog_to_celsius(const int &val, const temp_calibration &sensor) {
  float U = val * adc_stepsize;            // normalize reading
  float R = sensor.R_ref * (U / (1 - U));  // calculate resistance of thermocouple
  float x = log(R);                        // take log of resistance
  // evaluate the polynomial fit to get temperature
  return 1 / (sensor.a0 + x * (sensor.a1 + x * (sensor.a2 + x * sensor.a3))) - T0;
}

#endif
