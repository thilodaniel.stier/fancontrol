#ifndef PROFILE_H
#define PROFILE_H

#include <EEPROM.h>
#include "constants.h"
#include "temp_conversion.h"
#include "fancontrol.h"

#define RAM_SLOT_NUMBERS 10
#define EEPROM_SLOT_NUMBERS 4

// a struct containing all information set by user
struct profile {
  char name[16];                                      // some string describing the profile

  temp_calibration temp_calibrations[temp_count];     // calibration values for temperature sensors
  float temp_moving_average_factor;                   // weight of old temp value for moving average

  int source_temp[pwm_count];                         // temperature sensor to use for PWM control
  fancurve fancurves[pwm_count];                      // fancurves for sensors

  volatile int rpm_pulses_per_revolution[sense_count]; // number of signal changes per one revolution
  volatile float rpm_moving_average_factor;            // weight of old rpm value for moving average
  volatile unsigned long rpm_max_time;                 // max time to wait inbetween pulses [µs] (no pulse for longer is rounded to 0 rpm)
  volatile unsigned long rpm_min_time;                 // min time to wait inbetween pulses [µs] (for debouncing)
};

// hard-coded default profile as fallback and for debugging
const profile default_profile =
  {"default\0",
   {{2.68786525e-03, 2.80061491e-04, 3.65790765e-06, 1.57335154e-07, 9.85},
    {2.68786525e-03, 2.80061491e-04, 3.65790765e-06, 1.57335154e-07, 9.86},
    {2.68786525e-03, 2.80061491e-04, 3.65790765e-06, 1.57335154e-07, 9.87},
    {2.68786525e-03, 2.80061491e-04, 3.65790765e-06, 1.57335154e-07, 9.81},
    {2.68786525e-03, 2.80061491e-04, 3.65790765e-06, 1.57335154e-07, 9.81}},
   0.95,
   {0, 0, 0, 0, 0, 0, 0, 0, 0},
   {{{25, 30, 35, 40, 45, 60}, {0.3, 0.4, 0.5, 0.6, 0.7, 1.0}},
    {{25, 30, 35, 40, 45, 60}, {0.3, 0.4, 0.5, 0.6, 0.7, 1.0}},
    {{25, 30, 35, 40, 45, 60}, {0.3, 0.4, 0.5, 0.6, 0.7, 1.0}},
    {{25, 30, 35, 40, 45, 60}, {0.3, 0.4, 0.5, 0.6, 0.7, 1.0}},
    {{25, 30, 35, 40, 45, 60}, {0.3, 0.4, 0.5, 0.6, 0.7, 1.0}},
    {{25, 30, 35, 40, 45, 60}, {0.3, 0.4, 0.5, 0.6, 0.7, 1.0}},
    {{25, 30, 35, 40, 45, 60}, {0.3, 0.4, 0.5, 0.6, 0.7, 1.0}},
    {{25, 30, 35, 40, 45, 60}, {0.3, 0.4, 0.5, 0.6, 0.7, 1.0}},
    {{25, 30, 35, 40, 45, 60}, {0.3, 0.4, 0.5, 0.6, 0.7, 1.0}}},
   {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4},
   0.95,
   1000000,
   6000
};

// profiles stored in ram
profile profiles[RAM_SLOT_NUMBERS] = {default_profile, default_profile,
                                      default_profile, default_profile,
                                      default_profile, default_profile,
                                      default_profile, default_profile,
                                      default_profile, default_profile};

// pointer to the profile that is currently in use (default: 0)
profile *active_profile = profiles;
int active_profile_slot = 0;

// copy profile from source_slot to dest, both in range [-EEPROM_SLOT_NUMBERS, RAM_SLOT_NUMBERS-1]
// negative slots are stored in EEPROM, non negative ones are in RAM
// return whether successful (noting is changed if the function failes)
bool copy_profile(int dest_slot, int source_slot) {
  // make sure slot numbers are in correct range
  if (dest_slot < -EEPROM_SLOT_NUMBERS || dest_slot >= RAM_SLOT_NUMBERS ||
      source_slot < -EEPROM_SLOT_NUMBERS || source_slot >= RAM_SLOT_NUMBERS)
    return 0;

  // move within EEPROM
  if (dest_slot < 0 && source_slot < 0) {

    // start address of source
    int source = (source_slot + EEPROM_SLOT_NUMBERS) * sizeof(profile);
    // start address of dest
    int dest = (dest_slot + EEPROM_SLOT_NUMBERS) * sizeof(profile);

    // read from source and write to dest
    for (unsigned int i = 0; i < sizeof(profile); i++)
      EEPROM.write(dest + i, EEPROM.read(source + i));

  }
  // store into EEPROM
  else if (dest_slot < 0 && source_slot >= 0) {

    // pointer to start of source
    char *source = (char*) (profiles + source_slot);
    // start address of dest
    int dest = (dest_slot + EEPROM_SLOT_NUMBERS) * sizeof(profile);

    // write source into dest
    for (unsigned int i = 0; i < sizeof(profile); i++)
      EEPROM.write(dest + i, source[i]);

  }
  // load from EEPROM
  else if (dest_slot >= 0 && source_slot < 0) {

    // start address of source
    int source = (source_slot + EEPROM_SLOT_NUMBERS) * sizeof(profile);
    // pointer to start of dest
    char *dest = (char*) (profiles + dest_slot);

    // if the destination is the active profile, deactivate interrupts
    if (dest_slot == active_profile_slot)
      cli();
    
    // read from source and set dest
    for (unsigned int i = 0; i < sizeof(profile); i++)
      dest[i] = EEPROM.read(source + i);
    
    // reactivate interrupts
    sei();
  }
  // move within RAM
  else {

    // pointer to start of source
    char *source = (char*) (profiles + source_slot);
    // pointer to start of dest
    char *dest = (char*) (profiles + dest_slot);

    // if the destination is the active profile, deactivate interrupts
    if (dest_slot == active_profile_slot)
      cli();
    
    // copy source to dest
    memcpy(dest, source, sizeof(profile));

    // reactivate interrupts
    sei();
  }

  return 1;
}

// apply the profile at given slot (set as active), slot must be in RAM (>= 0)
// return whether successful (noting is changed if the function failes)
bool apply_profile(int slot) {
  // verify slot number range
  if (slot < 0 || slot >= RAM_SLOT_NUMBERS)
    return 0;

  cli();
  active_profile = profiles + slot;
  active_profile_slot = slot;
  sei();

  return 1;
}

#endif
