#ifndef CONSTANTS_H
#define CONSTANTS_H

const int pwm_freq = 25000;               // fan pwm frequency
const int pwm_res = 12;                   // fan pwm resolution
const int default_pwm_speed = 1000;       // default fan speed
const int max_pwm = 1 << pwm_res;         // maximal fan speed

const int adc_res = 12;                   // analog resolution (for temps)
const int max_adc = 1 << adc_res;         // maximal analog value
const float adc_stepsize = 1.0 / max_adc; // resolution of adc
const int adc_average_number = 1;         // number of analog samples to average over

#define PWM_PINS X(0,0) X(1,1) X(2,2) X(3,3) X(4,4) X(5,5) X(6,6) X(7,7) X(8,8)
#define SENSE_PINS X(0,24) X(1,25) X(2,26) X(3,27) X(4,28) X(5,29) X(6,30) X(7,31) X(8,32) X(9,33) X(10,34)
#define TEMP_PINS X(0,14) X(1,15) X(2,16) X(3,17) X(4,18)

#define X(NUM,PIN) PIN,

// pins with pwm control (fans)
const int pwm_pins[] = {PWM_PINS};
const int pwm_count = sizeof(pwm_pins) / sizeof(int);

// pins for rpm monitoring (fans + pumps)
const int sense_pins[] = {SENSE_PINS};
const int sense_count = sizeof(sense_pins) / sizeof(int);

// pins with analog temperature sensors
const int temp_pins[] = {TEMP_PINS};
const int temp_count = sizeof(temp_pins) / sizeof(int);

#undef X

#endif
