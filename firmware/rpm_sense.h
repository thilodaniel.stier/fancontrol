#ifndef RPM_SENSE_H
#define RPM_SENSE_H

#include "profile.h"
#include "constants.h"
#include "dynamic_state.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#define X(NUM,PIN)                                                      \
  void sense_interrupt_ ## NUM() {                                      \
    cli();                                                              \
    unsigned long time = micros();                                      \
    unsigned long diff = time - current_state->last_sense_change[NUM];  \
    current_state->last_sense_change[NUM] = time;                       \
    if (diff >= active_profile->rpm_min_time &&                         \
        diff <= active_profile->rpm_max_time) {                         \
      float cur_rpm = 60e6f / (diff * active_profile->rpm_pulses_per_revolution[NUM]); \
      current_state->rpms[NUM] =                                        \
        active_profile->rpm_moving_average_factor * current_state->rpms[NUM] + \
        (1 - active_profile->rpm_moving_average_factor) * cur_rpm;      \
    }                                                                   \
    sei();                                                              \
  }
SENSE_PINS
#undef X

void setup_sense_interrupts() {
#define X(NUM,PIN) attachInterrupt(digitalPinToInterrupt(PIN), sense_interrupt_ ## NUM, CHANGE);
  SENSE_PINS
#undef X
}

#endif
