#ifndef DYNAMIC_STATE_H
#define DYNAMIC_STATE_H

#include "constants.h"
#include "fancontrol.h"
#include "temp_conversion.h"
#include "profile.h"

// struct containing data that is derived from measurements and settings
struct dynamic_state {
  int temp_raw[temp_count];                        // values coming from adc of temp pins
  float temp[temp_count];                          // corresponding temperatures measured
  
  int pwm_vals[pwm_count];                         // current pwm values
  
  volatile float rpms[sense_count];                         // current fan rpms
  volatile unsigned long last_sense_change[sense_count];    // time of last sense pin change
};

dynamic_state *current_state;

void get_temps() {
  for (int i = 0; i < temp_count; i++) {
    int raw = analogRead(temp_pins[i]);
    current_state->temp_raw[i] = raw;
    float cur_temp = analog_to_celsius(raw, active_profile->temp_calibrations[i]);
    current_state->temp[i] =
      current_state->temp[i] * active_profile->temp_moving_average_factor
      + cur_temp * (1 - active_profile->temp_moving_average_factor);
  }
}

void set_pwms() {
  for (int i = 0; i < pwm_count; i++) {
    float temp = current_state->temp[active_profile->source_temp[i]];
    current_state->pwm_vals[i] = (int) (evaluate_fancurve(temp, active_profile->fancurves[i]) * max_pwm);
    analogWrite(pwm_pins[i], current_state->pwm_vals[i]);
  }
}

void check_rpms() {
  for (int i = 0; i < sense_count; i++) {
    cli();
    unsigned long time = micros();
    if (time - current_state->last_sense_change[i] > active_profile->rpm_max_time) {
      current_state->rpms[i] = 0;
      if (current_state->rpms[i] > 0)
        Serial.printf("reset rpm for %d (disconnect?)\n", i);
    }
    sei();
  }
  
}

void update_dynamic_state() {  
  get_temps();
  set_pwms();
  check_rpms();
}





#endif
