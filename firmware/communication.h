#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#include "profile.h"
#include "dynamic_state.h"

#include "usb_message.h"

// turn on led for ms millisecond
void led_signal(int ms) {
  static unsigned long off_time_ms = 0;
  
  if (ms > 0) {
    digitalWrite(13, HIGH);
    off_time_ms = millis() + ms;
  } else if (off_time_ms < millis()) {
    digitalWrite(13, LOW);
  }
}

void handle_usb() {
  // buffer for sending and receiving
  static char buffer[1024];
  // start address for payload (after message type header)
  static char* payload = buffer + sizeof(usb_message);
  static usb_message* message = (usb_message*) buffer;
  // current fill state of buffer (number of bytes filled)
  static unsigned int buffer_length = 0;

  
  // maximal number of iterations to wait for message completion
  const static int timeout = 1000;
  // number of iterations waited
  static int wait_iterations = 0;

  // if nothing is available, increase wait_iterations, otherwise set to 0
  if (!Serial.available())
    wait_iterations++;
  else
    wait_iterations = 0;

  // if waiting longer that timeout iterations, reset buffer (forget partial messages)
  if (wait_iterations > timeout)
    buffer_length = 0;

  
  // if there is no full message in the buffer, try to read
  while (Serial.available() && buffer_length < sizeof(usb_message))
      buffer[buffer_length++] = Serial.read();

  // if there is (still) no full message, return
  if (buffer_length < sizeof(usb_message))
    return;

  // switch the message (type)
  switch (*message) {
  case msg_get_dynamic_state: {

    // assemble return message...
    // message type
    *message = msg_send_dynamic_state;
    // add dynamic_state payload (in interrupt free zone, since it may be written to by interrupts)
    cli();
    memcpy(payload, current_state, sizeof(dynamic_state));
    sei();
    // set buffer length: message + dynamic_state payload
    buffer_length = sizeof(usb_message) + sizeof(dynamic_state);
    
    // send answer
    Serial.write(buffer, buffer_length);

    // reset buffer
    buffer_length = 0;
    
  } break;
  case msg_get_profile: {

    // receive int payload
    while (Serial.available() && buffer_length < sizeof(usb_message) + sizeof(int))
      buffer[buffer_length++] = Serial.read();

    // if message is not complete, return (to continue next time)
    if (buffer_length < sizeof(usb_message) + sizeof(int))
      return;

    // get int payload
    int slotnum = *((int*)payload);

    // if slot number is invalid...
    if (slotnum < -EEPROM_SLOT_NUMBERS || slotnum >= RAM_SLOT_NUMBERS) {
      // assemble return message...
      // message type
      *message = msg_nack;
      // set buffer length (just the message)
      buffer_length = sizeof(usb_message);
    }
    // if slot number is valid...
    else {
      // assemble return message...
      // message type
      *message = msg_send_profile;
      // first part of payload is slotnum, which is already in buffer
      // add profile at slotnum to buffer...
      // if requested profile is in EEPROM:
      if (slotnum < 0) {
        // get starting address of requested profile
        int source = (slotnum + EEPROM_SLOT_NUMBERS) * sizeof(profile);
        // read from EEPROM into buffer
        for (unsigned int i = 0; i < sizeof(profile); i++)
          payload[sizeof(int) + i] = EEPROM.read(source + i);
      }
      // if requested profile is in RAM:
      else {
        // just copy it into buffer
        memcpy(payload + sizeof(int), profiles + slotnum, sizeof(profile));
      }
      // set buffer length (message + slotnum + profile)
      buffer_length = sizeof(usb_message) + sizeof(int) + sizeof(profile);
    }
    
    // send answer
    Serial.write(buffer, buffer_length);

    // reset buffer
    buffer_length = 0;
    
  } break;
  case msg_send_profile: {
    
    // receive int + profile payload
    while (Serial.available() && buffer_length < sizeof(usb_message) + sizeof(int) + sizeof(profile))
      buffer[buffer_length++] = Serial.read();

    // if the message is not complete, return (to continue next time)
    if (buffer_length < sizeof(usb_message) + sizeof(int) + sizeof(profile))
      return;

    // get int payload
    int slotnum = *((int*)payload);

    // if slot number is invalid...
    if (slotnum < -EEPROM_SLOT_NUMBERS || slotnum >= RAM_SLOT_NUMBERS) {
      // assemble return message...
      // message type
      *message = msg_nack;
      // set buffer length (just the message)
      buffer_length = sizeof(usb_message);
    }
    // if slot number is valid...
    else {
      // write profile to slot...
      // if the profile should be stored in EEPROM:
      if (slotnum < 0) {
        // get start address of slot
        int dest = (slotnum + EEPROM_SLOT_NUMBERS) * sizeof(profile);
        // write profile payload to EEPROM
        for (unsigned int i = 0; i < sizeof(profile); i++)
          EEPROM.write(dest + i, payload[sizeof(int) + i]);
      }
      // if the profile should be stored in RAM:
      else {
        // if the specified slot number is currently in use, deactivate interrupts
        if (slotnum == active_profile_slot)
          cli();
        // copy the profile from the buffer to the specified slot
        memcpy(profiles + slotnum, payload + sizeof(int), sizeof(profile));
        // reactivate interrupts
        sei();
      }
      
      // assemble return message...
      // message type
      *message = msg_ack;
      // set buffer length (just the message)
      buffer_length = sizeof(usb_message);
    }
    
    // send answer
    Serial.write(buffer, buffer_length);
    Serial.flush();
    
    // reset buffer
    buffer_length = 0;
    
  } break;
  case msg_move_profile: {

    // receive int + int payload
    while (Serial.available() && buffer_length < sizeof(usb_message) + sizeof(int) + sizeof(int))
      buffer[buffer_length++] = Serial.read();

    // if the message is not complete, return (to continue next time)
    if (buffer_length < sizeof(usb_message) + sizeof(int) + sizeof(int))
      return;

    // get the int payloads
    int dest = *((int*)payload);
    int source = *((int*)(payload + sizeof(int)));

    // if copy is successful (check for allowed slot numbers is done in copy_profile):
    if (copy_profile(dest, source)) {
      // assemble message...
      // message type
      *message = msg_ack;
      // set length (message only)
      buffer_length = sizeof(usb_message);
    }
    // if copy failed:
    else {
      // assemble message...
      // message type
      *message = msg_nack;
      // set length (message only)
      buffer_length = sizeof(usb_message);
    }
    
    // send answer
    Serial.write(buffer, buffer_length);
    Serial.flush();
    
    // reset buffer
    buffer_length = 0; 
    
  } break;
  case msg_get_active_slot: {
    
    // assemble return message...
    // message type
    *message = msg_send_active_slot;
    // set int payload (slot number)
    *((int*)payload) = active_profile_slot;
    
    // set buffer length: message + int payload
    buffer_length = sizeof(usb_message) + sizeof(int);

    // send answer
    Serial.write(buffer, buffer_length);
    Serial.flush();
    
    // reset buffer
    buffer_length = 0;
    
  } break;
  case msg_send_active_slot: {

    // receive int payload
    while (Serial.available() && buffer_length < sizeof(usb_message) + sizeof(int))
      buffer[buffer_length++] = Serial.read();

    // if the message is not complete, return (to continue next time)
    if (buffer_length < sizeof(usb_message) + sizeof(int))
      return;

    // get the int payload
    int slotnum = *((int*)payload);

    // if setting active slot is successful:
    if (apply_profile(slotnum)) {
      // assemble message...
      // message type
      *message = msg_ack;
      // set length (message only)
      buffer_length = sizeof(usb_message);
    }
    // if setting active slot failed:
    else {
      // assemble message...
      // message type
      *message = msg_nack;
      // set length (message only)
      buffer_length = sizeof(usb_message);
    }
    
    // send answer
    Serial.write(buffer, buffer_length);
    Serial.flush();
    
    // reset buffer
    buffer_length = 0;
    
  } break;
  default: {

    // assemble return message:
    // message type
    *message = msg_error;
    // set length (message only)
    buffer_length = sizeof(usb_message);

    // send answer
    Serial.write(buffer, buffer_length);
    Serial.flush();
    
    // empty read buffer
    while (Serial.available())
      Serial.read();
    
    // reset buffer
    buffer_length = 0;

    // send led signal to verify communication / show error
    led_signal(100);
  }
  }
}


#endif
