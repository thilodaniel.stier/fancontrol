#ifndef PROFILE_EDIT_WINDOW
#define PROFILE_EDIT_WINDOW

#include <bits/stdc++.h>
#include <ncurses.h>
#include "device_communication.hpp"

class profile_edit_window {
  // profile to edit
  profile p;
  
  // original of profile
  profile original;
  
  // slot of profile (just for display reasons)
  int slot;

  // done editing
  bool done = 0;

  // (user) cursor position as shown on screen
  int cursor_x = 32, cursor_y = 3;
  
  // virtual x-coordinate of cursor (to stay in the same column when less are avaiable)
  int virtual_cursor_x = 32;

  // temp calibrations being edited currently (mask)
  int editing_temp_calibration = 0;

  // fancurves being edited currently (mask)
  int editing_fancurve = 0;
  
  // positions where the (user) cursor is allowed to be when editing temp calibration
  std::bitset<80> allowed_positions_temp[24] =
    {std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010001000100010001000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010001000100010001000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010001000100010001000100010000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00010000000000000001000000000000000000000100000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80)};

  // positions where the (user) cursor is allowed to be when editing fancurve
  std::bitset<80> allowed_positions_fancurve[24] =
    {std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010001000100010001000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010001000100010001000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010001000100010001000100010000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000010000001000000100000010000001000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000010000001000000100000010000001000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00010000000000000001000000000000000000000100000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80)};

  // positions where the (user) cursor is allowed to be
  std::bitset<80> allowed_positions_default[24] =
    {std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010001000100010001000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010001000100010001000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100010001000100010001000100010001000100010000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000100000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80),
     std::bitset<80>("00010000000000000001000000000000000000000100000000000000000000000000000000000000", 80),
     std::bitset<80>("00000000000000000000000000000000000000000000000000000000000000000000000000000000", 80)};

  std::bitset<80> *allowed_positions;

  // history of input buffers; at position 0 is the active one, if there is one
  std::deque<std::string> input_history;
  // position in input history currently viewed if >= 0
  int input_history_pointer = -1;

  // error string displayed on screen
  std::string last_error;

  // return whether the temp calibration submenu is open
  bool temp_submenu_open() {
    return editing_temp_calibration != 0;
  }

  // return whether the fancurve submenu is open
  bool fancurve_submenu_open() {
    return editing_fancurve != 0;
  }

  void move_cursor_up() {
    int cur_x = cursor_x, cur_y = cursor_y;
    // go up one and put cursor on virtual x
    cursor_y = (cursor_y + 23) % 24;
    cursor_x = virtual_cursor_x;
    // search left and then up for allowed position
    while (!allowed_positions[cursor_y][79 - cursor_x]) {
      if (cursor_x == 0) 
        cursor_x = virtual_cursor_x, cursor_y = (cursor_y + 23) % 24;
      else
        cursor_x--;
    }
    // if we landed where we started, we want to search right instead of left
    if (cursor_x == cur_x && cursor_y == cur_y) {
      // go up one and put cursor on virtual x
      cursor_y = (cursor_y + 23) % 24;
      cursor_x = virtual_cursor_x;
      // search right and then up for allowed position
      while (!allowed_positions[cursor_y][79 - cursor_x]) {
        if (cursor_x == 79) 
          cursor_x = virtual_cursor_x, cursor_y = (cursor_y + 23) % 24;
        else
          cursor_x++;
      }
      // set virtual x to found position to be able to search up nicely
      virtual_cursor_x = cursor_x;
    }
  }
  void move_cursor_down() {
    int cur_x = cursor_x, cur_y = cursor_y;
    // go down one and put cursor on virtual x
    cursor_y = (cursor_y + 1) % 24;
    cursor_x = virtual_cursor_x;
    // search left and then down for allowed position
    while (!allowed_positions[cursor_y][79 - cursor_x]) {
      if (cursor_x == 0)
        cursor_x = virtual_cursor_x, cursor_y = (cursor_y + 1) % 24;
      else
        cursor_x--;
    }
    // if we landed where we started, we want to search right instead of left
    if (cursor_x == cur_x && cursor_y == cur_y) {
      // go up one and put cursor on virtual x
      cursor_y = (cursor_y + 1) % 24;
      cursor_x = virtual_cursor_x;
      // search right and then up for allowed position
      while (!allowed_positions[cursor_y][79 - cursor_x]) {
        if (cursor_x == 79) 
          cursor_x = virtual_cursor_x, cursor_y = (cursor_y + 1) % 24;
        else
          cursor_x++;
      }
      // set virtual x to found position to be able to search up nicely
      virtual_cursor_x = cursor_x;
    }
  }
  void move_cursor_left() {
    // go left until valid position is found; reset virtual x to real x
    while (!allowed_positions[cursor_y][79 - (virtual_cursor_x = cursor_x = (cursor_x + 79) % 80)]) ;
  }
  void move_cursor_right() {
    // go right until valid position is found; reset virtual x to real x
    while (!allowed_positions[cursor_y][79 - (virtual_cursor_x = cursor_x = (cursor_x + 1) % 80)]) ;
  }
  
public:
  profile_edit_window(profile p, int slot) : p(p), slot(slot) {
    original = p;
    allowed_positions = allowed_positions_default;
  }

  void draw_screen() {
    // clear screen
    erase();

    // draw border
    {
      mvaddch(0,0,ACS_ULCORNER);
      mvaddch(0,80-1,ACS_URCORNER);
      mvaddch(24-1,0,ACS_LLCORNER);
      mvaddch(24-1,80-1,ACS_LRCORNER);
      for (int i = 1; i < 80-1; i++) {
        mvaddch(0, i, ACS_HLINE);
        mvaddch(24-1,i, ACS_HLINE);
      }
      for (int i = 1; i < 24-1; i++) {
        mvaddch(i, 0, ACS_VLINE);
        mvaddch(i, 80-1, ACS_VLINE);
      }
    }

    // draw title bar
    {
      mvprintw(1, 2, "Edit profile %d", slot);

      for (int i = 1; i < 80-1; i++)
        mvaddch(2, i, ACS_HLINE);

      mvaddch(2, 0, ACS_LTEE);
      mvaddch(2, 79, ACS_RTEE); 
    }

    // draw profile text
    {
      mvprintw(3, 2, "%-28s [%s]",
               "name:",
               p.name);
      
      mvprintw(4, 2, "%-28s [ ] [ ] [ ] [ ] [ ]",
               "temp calibrations:");
      
      mvprintw(5, 2, "%-28s [%1.5f]",
               "temp moving average factor:",
               p.temp_moving_average_factor);
      
      mvprintw(6, 2, "%-28s [%d] [%d] [%d] [%d] [%d] [%d] [%d] [%d] [%d]",
               "source temp:",
               p.source_temp[0],
               p.source_temp[1],
               p.source_temp[2],
               p.source_temp[3],
               p.source_temp[4],
               p.source_temp[5],
               p.source_temp[6],
               p.source_temp[7],
               p.source_temp[8]);
      
      mvprintw(7, 2, "%-28s [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ] [ ]",
               "fancurves:");
      
      mvprintw(8, 2, "%-28s [%d] [%d] [%d] [%d] [%d] [%d] [%d] [%d] [%d] [%d] [%d]",
               "rpm pulses per revolution:",
               p.rpm_pulses_per_revolution[0],
               p.rpm_pulses_per_revolution[1],
               p.rpm_pulses_per_revolution[2],
               p.rpm_pulses_per_revolution[3],
               p.rpm_pulses_per_revolution[4],
               p.rpm_pulses_per_revolution[5],
               p.rpm_pulses_per_revolution[6],
               p.rpm_pulses_per_revolution[7],
               p.rpm_pulses_per_revolution[8],
               p.rpm_pulses_per_revolution[9],
               p.rpm_pulses_per_revolution[10]);

      mvprintw(9, 2, "%-28s [%1.5f]",
               "rpm moving average factor",
               p.rpm_moving_average_factor);

      mvprintw(10, 2, "%-28s [%d]",
               "rpm max time",
               p.rpm_max_time);
      
      mvprintw(11, 2, "%-28s [%d]",
               "rpm min time",
               p.rpm_min_time);
    }

    // draw profile/submenu divider
    {
      for (int i = 1; i < 80-1; i++)
        mvaddch(12, i, ACS_HLINE);
      
      mvaddch(12, 0, ACS_LTEE);
      mvaddch(12, 79, ACS_RTEE); 
    }

    // draw temp calibration submenu if needed
    if (temp_submenu_open()) {
      // selected indices
      std::vector<int> sel;
      for (int i = 0; i < temp_count; i++)
        if ((editing_temp_calibration >> i) & 1)
          sel.push_back(i);
      
      for (int i : sel)
        mvaddch(4, 32 + i * 4, '#');
      
      mvprintw(13, 2, "temp calibration[s]");

      
      // are all a0 values equal?
      bool tmp = 1;
      float a0 = p.temp_calibrations[sel[0]].a0;
      for (int i : sel)
        if (p.temp_calibrations[i].a0 != a0)
          tmp = 0;

      // if all a0 values are equal, print them
      if (tmp)
        mvprintw(14, 2, "%-28s [%.8e]", "a0:", a0);
      // otherwise print empty field
      else
        mvprintw(14, 2, "%-28s [        ]", "a0:", a0);

      // are all a1 values equal?
      tmp = 1;
      float a1 = p.temp_calibrations[sel[0]].a1;
      for (int i : sel)
        if (p.temp_calibrations[i].a1 != a1)
          tmp = 0;

      // if all a1 values are equal, print them
      if (tmp)
        mvprintw(15, 2, "%-28s [%.8e]", "a1:", a1);
      // otherwise print empty field
      else
        mvprintw(15, 2, "%-28s [        ]", "a1:", a1);

      // are all a2 values equal?
      tmp = 1;
      float a2 = p.temp_calibrations[sel[0]].a2;
      for (int i : sel)
        if (p.temp_calibrations[i].a2 != a2)
          tmp = 0;

      // if all a2 values are equal, print them
      if (tmp)
        mvprintw(16, 2, "%-28s [%.8e]", "a2:", a2);
      // otherwise print empty field
      else
        mvprintw(16, 2, "%-28s [        ]", "a2:", a2);

      // are all a3 values equal?
      tmp = 1;
      float a3 = p.temp_calibrations[sel[0]].a3;
      for (int i : sel)
        if (p.temp_calibrations[i].a3 != a3)
          tmp = 0;

      // if all a3 values are equal, print them
      if (tmp)
        mvprintw(17, 2, "%-28s [%.8e]", "a3:", a3);
      // otherwise print empty field
      else
        mvprintw(17, 2, "%-28s [        ]", "a3:", a3);

      // are all R_ref values equal?
      tmp = 1;
      float R_ref = p.temp_calibrations[sel[0]].R_ref;
      for (int i : sel)
        if (p.temp_calibrations[i].R_ref != R_ref)
          tmp = 0;

      // if all R_ref values are equal, print them
      if (tmp)
        mvprintw(18, 2, "%-28s [%6.3f]", "R_ref [k]:", R_ref);
      // otherwise print empty field
      else
        mvprintw(18, 2, "%-28s [      ]", "R_ref [k]:", R_ref);
      
    }
    // draw fancurve submenu if needed
    else if (fancurve_submenu_open()) {

      // selected indices
      std::vector<int> sel;
      for (int i = 0; i < pwm_count; i++)
        if ((editing_fancurve >> i) & 1)
          sel.push_back(i);

      // add selection markers
      for (int i : sel)
        mvaddch(7, 32 + i * 4, '#');

      // add title
      mvprintw(13, 2, "editing fancurve[s]");

      // add temps label
      mvprintw(14, 2, "%-28s ", "temps [°C]:");

      // add temp boxes
      for (int j = 0; j < 6; j++) {
        // are temps equal for all?
        bool tmp = 1;
        float tempj = p.fancurves[sel[0]].temps[j];
        for (int i : sel)
          if (p.fancurves[i].temps[j] != tempj)
            tmp = 0;

        // if value is equal for all selected fancurves, print it
        if (tmp)
          printw("[%4.1f] ", tempj);
        // otherwise print empty field
        else
          printw("[    ] ");
      }
      
      mvprintw(15, 2, "%-28s ",
               "speeds [% PWM]:");
      for (int j = 0; j < 6; j++) {
        
        // are pwm vals equal for all?
        bool tmp = 1;
        float pwmj = p.fancurves[sel[0]].speeds[j] * 100;
        for (int i : sel)
          if (p.fancurves[i].speeds[j] * 100 != pwmj)
            tmp = 0;

        // if value is equal for all selected fancurves, print it
        if (tmp) {
          // write number into string first to truncate 100.0 to 100. so that it fits into 4 chars
          char tmp[6];
          sprintf(tmp, "%4.1f", pwmj);
          if (strlen(tmp) > 4)
            tmp[4] = 0;
          printw("[%4s] ", tmp);
        }
        // otherwise print empty field
        else {
          printw("[    ] ");
        }
      }
    }
    
    // draw submenu/footer divider
    {
      for (int i = 1; i < 80-1; i++)
        mvaddch(21, i, ACS_HLINE);
      
      mvaddch(21, 0, ACS_LTEE);
      mvaddch(21, 79, ACS_RTEE); 
    }

    // draw footer
    {
      mvprintw(22, 2, "[save and exit]");
      mvaddch(22, 3, 's' | A_UNDERLINE);
      
      mvprintw(22, 18, "[exit without saving]");
      mvaddch(22, 20, 'x' | A_UNDERLINE);
      
      mvprintw(22, 40, "[revert]");
      mvaddch(22, 41, 'r' | A_UNDERLINE);
    }

    // draw input buffer
    {
      mvaddch(21, 49, ACS_TTEE);
      mvaddch(22, 49, ACS_VLINE);
      mvaddch(23, 49, ACS_BTEE);

      // draw current input
      if (input_history_pointer >= 0 && input_history_pointer < (int)input_history.size())
        mvprintw(22, 51, "%-27s", input_history[input_history_pointer].c_str());
    }

    // draw error string
    {
      mvprintw(20, 2, "%76s", last_error.c_str());
    }
    
    move(cursor_y, cursor_x);

    // if in input mode, draw cursor in input buffer window
    if (input_history_pointer >= 0 && input_history_pointer < (int)input_history.size())
      move(22, 51 + input_history[input_history_pointer].length());
  }
  
  // handle keystrokes to navigate window
  void handle_key(int key) {

    // set the currently relevant allowed positions 
    if (temp_submenu_open())
      allowed_positions = allowed_positions_temp;
    else if (fancurve_submenu_open())
      allowed_positions = allowed_positions_fancurve;
    else
      allowed_positions = allowed_positions_default;

    static int last_key;
    char tmp[20];
    sprintf(tmp, "%d %d", last_key, key);
    last_error = tmp;
    last_key = key;
    
    
    switch (key) {
    case 27: /* Alt+Key combinations */ {
      int next_key = getch();
      switch (next_key) {
      case 's': /* save and exit */ {
        virtual_cursor_x = cursor_x = 3;
        cursor_y = 22;
        
      } break;
      case 'x': /* exit without saving */ {
        virtual_cursor_x = cursor_x = 19;
        cursor_y = 22;
        
      } break;
      case 'r': /* revert */ {
        virtual_cursor_x = cursor_x = 41;
        cursor_y = 22;
        
      } break;
      }
    } break;
    case KEY_UP: {

      // if in input mode...
      if (input_history_pointer >= 0 && input_history_pointer < (int)input_history.size()) {
        // go up in history (if possible)
        input_history_pointer = std::min(input_history_pointer + 1, (int)input_history.size() - 1);
        break;
      }
      
      move_cursor_up();
      
    } break;
    case KEY_DOWN: {

      // if in input mode...
      if (input_history_pointer >= 0 && input_history_pointer < (int)input_history.size()) {
        // go down in history (if possible)
        input_history_pointer = std::max(input_history_pointer - 1, 0);
        break;
      }
      
      move_cursor_down();
      
    } break;
    case KEY_LEFT: {
      
      // if in input mode...
      if (input_history_pointer >= 0 && input_history_pointer < (int)input_history.size()) {
        // don't process left (for now)
        break;
      }
      
      move_cursor_left();

    } break;
    case KEY_HOME: {

      // if in input mode...
      if (input_history_pointer >= 0 && input_history_pointer < (int)input_history.size()) {
        // don't process home key (for now)
        break;
      }
      
      // find first allowed position from the left
      cursor_x = 79;
      while (!allowed_positions[cursor_y][79 - (virtual_cursor_x = cursor_x = (cursor_x + 1) % 80)]);
      
    } break;
    case KEY_RIGHT: {
      
      // if in input mode...
      if (input_history_pointer >= 0 && input_history_pointer < (int)input_history.size()) {
        // don't process right (for now)
        break;
      }
      
      move_cursor_right();
      
    } break;
    case KEY_END: {

      // if in input mode...
      if (input_history_pointer >= 0 && input_history_pointer < (int)input_history.size()) {
        // don't process end key (for now)
        break;
      }
      
      // find first allowed position from the right
      cursor_x = 0;
      while (!allowed_positions[cursor_y][79 - (virtual_cursor_x = cursor_x = (cursor_x + 79) % 80)]);

    } break;
    case ' ': {
      // if in input mode...
      if (input_history_pointer == 0) {
        // if in new input buffer, write space
        if (input_history_pointer == 0)
          input_history[0] += ' ';
        break;
      }
    }
    case '\n': {
      // if we are in input mode...
      if (input_history_pointer >= 0) {

        // get input string
        std::string s = input_history[input_history_pointer];

        // reset error string (so it isn't displayed after successful conversion)
        last_error = "";
        // handle conversions, checks and assignments
        try {
          // we know which field should get input based on cursor position
          switch (cursor_y) {
          case 3: /* name edit */ {

            // check length; truncate if too long
            if (s.length() > 15)
              s = s.substr(0,15);
            // set name
            strcpy(p.name, s.c_str());
            
          } break;
          case 5: /* temp moving average */ {

            // get the float value
            float f = stof(s);

            // set if it is in valid range
            if (f < 1 && f >= 0)
              p.temp_moving_average_factor = f;
            else
              throw std::out_of_range("moving average factor has to be in range [0,1[");
            
          } break;
          case 6: /* source temp */ {

            // get the int value
            int i = stoi(s);

            // set it if it is in valid range
            if (i >= 0 && i <= 4)
              p.source_temp[(cursor_x-32) / 4] = i;
            else
              throw std::out_of_range("source temp must be an integer in range [0,4]");
            
          } break;
          case 8: /* rpm pulses / rev */ {

            // get the int value
            int i = stoi(s);
            
            // set it if it is in valid range
            if (i >= 1)
              p.rpm_pulses_per_revolution[(cursor_x-32) / 4] = i;
            else
              throw std::out_of_range("rpm pulses per rev has to be an integer >= 1");
            
          } break;
          case 9: /* rpm moving average factor */ {
            
            // get the float value
            float f = stof(s);

            // set if it is in valid range
            if (f < 1 && f >= 0)
              p.rpm_moving_average_factor = f;
            else
              throw std::out_of_range("moving average factor has to be in range [0,1[");
            
          } break;
          case 10: /* rpm max time */ {
            
            // get the int value
            int i = stoi(s);

            // set if it is in valid range
            if (i > 0)
              p.rpm_max_time = i;
            else
              throw std::out_of_range("rpm max time must be an int > 0");
            
          } break;
          case 11: /* rpm min time */ {
            
            // get the int value
            int i = stoi(s);

            // set if it is in valid range
            if (i > 0)
              p.rpm_min_time = i;
            else
              throw std::out_of_range("rpm min time must be an int > 0");
            
          } break;
          default: /* we are in a submenu */ {
            if (fancurve_submenu_open()) /* editing a fancurve field */ {
              // selected indices
              std::vector<int> sel;
              for (int i = 0; i < pwm_count; i++)
                if ((editing_fancurve >> i) & 1)
                  sel.push_back(i);

              switch (cursor_y) {
              case 14: /* temps */ {

                // get the float value
                float f = stof(s);

                // set it (no need to check ranges, anything is ok)
                for (int i : sel)
                  p.fancurves[i].temps[(cursor_x - 32) / 7] = f;

                // maybe todo: sort fancurve by temp values
                
              } break;
              case 15: /* speeds */ {

                // get the float value
                float f = stof(s);

                // set if it is in valid range
                if (f >= 0 && f <= 100)
                  for (int i : sel)
                    p.fancurves[i].speeds[(cursor_x - 32) / 7] = f / 100;
                else
                  throw std::out_of_range("pwm speed has to be in range [0,100]");
                
              } break;
              }
            }
            else if (temp_submenu_open()) /* editing temp cali */ {
              // selected indices
              std::vector<int> sel;
              for (int i = 0; i < temp_count; i++)
                if ((editing_temp_calibration >> i) & 1)
                  sel.push_back(i);

              switch (cursor_y) {
              case 14: /* a0 */ {
                
                // get the float value
                float f = stof(s);
                
                // set it (no need to check ranges, anything is ok (theoretically))
                for (int i : sel)
                  p.temp_calibrations[i].a0 = f;
                
              } break;
              case 15: /* a1 */ {
                
                // get the float value
                float f = stof(s);
                
                // set it (no need to check ranges, anything is ok (theoretically))
                for (int i : sel)
                  p.temp_calibrations[i].a1 = f;

              } break;
              case 16: /* a2 */ {
                
                // get the float value
                float f = stof(s);
                
                // set it (no need to check ranges, anything is ok (theoretically))
                for (int i : sel)
                  p.temp_calibrations[i].a2 = f;

              } break;
              case 17: /* a3 */ {
                
                // get the float value
                float f = stof(s);
                
                // set it (no need to check ranges, anything is ok (theoretically))
                for (int i : sel)
                  p.temp_calibrations[i].a3 = f;

              } break;
              case 18: /* R_ref */ {
                
                // get the float value
                float f = stof(s);

                // set it if it is in valid range
                if (f > 0)
                  for (int i : sel)
                    p.temp_calibrations[i].R_ref = f;
                else
                  throw std::out_of_range("R_ref has to be > 0");

              } break;
              }
            }
            else /* this should not happen */ {
              ; // todo throw some descriprive exception
            }
          } break;
          }
        } catch (std::exception &ex) /* failed to convert to valid value */ {
          last_error = ex.what();
        }

        // if we are using an item from history, remove the new one (only used inputs stay in history)
        if (input_history_pointer > 0)
          input_history.pop_front();
        // finish input mode
        input_history_pointer = -1;
      }
      // save and exit
      else if (cursor_x == 3 && cursor_y == 22) {
        done = 1;
      }
      // exit without saving
      else if (cursor_x == 19 && cursor_y == 22) {
        p = original;
        done = 1;
      }
      // revert
      else if (cursor_x == 41 && cursor_y == 22) {
        p = original;
      }
      // edit name, temp moving average factor, source temp, rpm pulses per revolution, rpm moving average factor, rpm max time, rpm min time or something in the subwindow
      else if (cursor_y == 3 ||
               cursor_y == 5 ||
               cursor_y == 6 ||
               cursor_y == 8 ||
               cursor_y == 9 ||
               cursor_y == 10 ||
               cursor_y == 11 ||
               cursor_y == 14 ||
               cursor_y == 15 ||
               cursor_y == 16 ||
               cursor_y == 17 ||
               cursor_y == 18) {
        // start new input
        input_history.push_front("");
        input_history_pointer = 0;
      }
      // edit temp calibrations
      else if (cursor_y == 4) {
        // toggle the one at cursor position
        editing_temp_calibration ^= 1 << ((cursor_x - 32) / 4);
        // deactivate editing fancurves
        editing_fancurve = 0;
      }
      // edit fancurves
      else if (cursor_y == 7) {
        // toggle the one at cursor position
        editing_fancurve ^= 1 << ((cursor_x - 32) / 4);
        // deactivate editing temp calibrations
        editing_temp_calibration = 0;
      }
      
    } break;
    case KEY_BACKSPACE: {
      // if in input mode and fresh buffer, remove last char
      if (input_history_pointer == 0 && input_history[0].size() > 0)
        input_history[0].pop_back();

    } break;
    default: {
      // if input mode in new buffer...
      if (input_history_pointer == 0) {
        // if the key (char) is printable, add it to input buffer
        if (isprint(key) && key > 0 && key <= 255) {
          input_history[input_history_pointer] += (char)key;
        }
      }
    } break;
    }
    
  }

  // a function that returns whether the window is done and should be closed by it's parent
  bool is_done() {
    return done;
  }

  // get the (edited) profile
  profile get_profile() {
    return p;
  }
};

#endif
